# Contributing content

### Blog posts

The website uses the same Markdown syntax.

1. When ready, write the full blog post!
    - Feel free to use any of the formatting and Markdown syntax available on the forum. Use this [Markdown tutorial](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) to see other markdown syntax you can use for formatting.
    - Include as many images as you like (NB: licensing issues, and where you got them from).

2. When you're finished, we will help you to transfer the text to the website!
    - If you want to be attributed, we need to add your contact details to [authors.yml](_data/authors.yml), including your photo 290px by 310px or similar aspect ratio.

All your work is attributed to you and has your name attached to it (unless you want to stay anonymous, of course). You are the copyright owner (the license is [CC-BY](https://creativecommons.org/licenses/by/4.0/)).


### Other content

If you're looking for content to contribute, first file an issue to the website's code repository for discussion and to get consent from the website maintainers.

### Previous / Next navigation

The website uses a pagination scheme (see [post_pagination.html](_includes/post_pagination.html)) that drives the 'Previous' and 'Next' buttons at the bottom of the page.

When adding new pages to the website this pagination needs to be tested to see if there is still a logical ordering of the page. If this is not the case, then the automatic pagination must be overridden by adding `custom_pagination` to the frontmatter metadata and specify either the `prev` or `next` page (or both) manually.

Example:

```yaml
permalink: /about/
title: "About"
excerpt: "About innercircles"
author_profile: false
last_modified_at: 2020-04-12T11+02:00
sidebar:
  nav: "about"
custom_pagination:
  prev: /resources/
---
```

(Adopted from [Humane Tech Community](https://humanetech.community) website. See their repository [Issue #28](https://github.com/humanetech-community/community-hub/issues/28) and [Pull Request #62](https://github.com/humanetech-community/community-hub/pull/62) for background information.)
